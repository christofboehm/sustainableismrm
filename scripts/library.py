import requests
import urllib.request
import csv

from bs4 import BeautifulSoup

from geopy.distance import geodesic
from geopy.geocoders import Nominatim

def getnAccpetedAbstracts(year):
    baseURL = 'HTTP://archive.ismrm.org/'
    URLroot = 'index.html'
    url = baseURL + year + '/' + URLroot
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'lxml')
    line = soup.findAll('li')[5]
    return int(line.string[-4:])

def getFirstAuthorLocation(url, abstractID):
    year = url.split('/')[-1]

    url = url + '/' + abstractID + '.html'
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'lxml')

    if int(year) > 2015:
        line = soup.find("div", {"id": "affOther"})
    elif int(year) == 2015 or int(year) == 2014:
        line = soup.find("i")
    else:
        line = soup.find("p", {"class": "ISMRMAffiliations"})

    ManipulateString = line.get_text(strip=True, separator=" <sup> ")
    ManipulateString = ManipulateString.split('<sup>')[1]
    ManipulateString = ManipulateString.split(',')

    ManipulateString = [item.strip() for item in ManipulateString]
    ManipulateString = list(filter(None, ManipulateString))

    ManipulateString = [w.replace('\t', '') for w in ManipulateString]
    ManipulateString = [w.replace('\n', '') for w in ManipulateString]
    ManipulateString = [w.replace(';', '') for w in ManipulateString]

    if 'United States' in ManipulateString or 'Canada' in ManipulateString \
       or 'USA' in ManipulateString:
        ManipulateString = ManipulateString[-3:]
    else:
        ManipulateString = ManipulateString[-2:]

    myString = ", ".join(ManipulateString)
    return myString 

def getRefLocation(year):
    if year == '2010':
        location = "Stockholm, Sweden"
    elif year == '2011':
        location = "Montreal, Canada"
    elif year == '2012':
        location = "Melbourne, Australia"
    elif year == '2013':
        location = "Salt Lake City, Utah, USA"
    elif year == '2014':
        location = "Milan, Italy"
    elif year == '2015':
        location = "Toronto, Ontario, Canada"
    elif year == '2016':
        location = "Singapore"
    elif year == '2017':
        location = "Honolulu, HI, USA"
    elif year == '2018':
        location = "Paris, France"
    elif year == '2019':
        location = "Montreal, Canada"

    geolocator = Nominatim(user_agent = 'my_app', timeout=20)
    ref_geolocation = geolocator.geocode(location)
    ref_altlong = ((ref_geolocation.latitude, ref_geolocation.longitude))
    return ref_altlong

def nExtracted(filename):
    csv_file = open(filename, 'r')
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    for row in csv_reader:
        line_count += 1

    line_count = round(line_count / 2)
    csv_file.close()
    return line_count

def getTotalMilesFromFile(filename, limit):
    csv_file = open(filename, 'r')
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    totalmiles = 0

    for row in csv_reader:
        if (line_count % 2) == 0:
            miles = float(row[-1])
            if miles > limit:
                totalmiles += miles
        line_count += 1

    csv_file.close()
    return round(totalmiles)

def getAllLongAltFromFile(filename):
    longalt_array = []
    csv_file = open(filename, 'r')
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    totalmiles = 0

    for row in csv_reader:
        if (line_count % 2) == 0:
            longitude = float(row[2])
            altitude = float(row[3])
            longalt_array.append((longitude, altitude))
        line_count += 1

    return longalt_array

def nAbstractsFromCountry(filename, test_country):
    csv_file = open(filename, 'r')
    csv_reader = csv.reader(csv_file, delimiter=',')
    line_count = 0
    country_count = 0

    for row in csv_reader:
        if (line_count % 2) == 0:
            country = row[1]
            if country == test_country:
                country_count += 1
        line_count += 1

    csv_file.close()
    return country_count

