import library as lib

from geopy.geocoders import Nominatim
from geopy.distance import geodesic

years = ['2010', '2012', '2013', '2014', '2015', '2016', '2017', \
         '2018', '2019'];

#### Define necessary constants ####
miles2kilometer = 1.60934
limitnoflight = 300 # limit for short distances
co2perkm_kg = 0.211 # see literature.bib file 'Co2permile2'

#### Per Year analysis ####
# output goes do defined csv files, write: Year, #AcceptedAbstracts,
# #extractedAbstracts, #totalmiles, #extrapolated totalmiles, miles per person,
# totalco2, co2perperson
writefile= '../data/processed/analysis.csv'
with open(writefile, 'w') as f:
    for year in years:
        print('Processing year: %s'%(year))
        filename = '../data/processed/%s.csv'%(year)

        totalmiles = lib.getTotalMilesFromFile(filename, limitnoflight)
        totalmiles *= 2 # return flight
        nAccepted = lib.getnAccpetedAbstracts(year)
        nExtract = lib.nExtracted(filename)
        factor = nAccepted / nExtract
        miles_perPerson = round(totalmiles / nExtract)
        extrapolated_totalmiles = totalmiles * factor

        totalkilometers = round(extrapolated_totalmiles * miles2kilometer)
        totalco2 = totalkilometers * co2perkm_kg
        co2perPerson = totalco2 / nAccepted

        writestring = year +','+ str(nAccepted) +','+ \
            str(nExtract) +','+ str(totalmiles) +','+ \
            str(round(extrapolated_totalmiles))\
            +','+ str(miles_perPerson) +',' + str(round(totalco2)) +','+ \
            str(round(co2perPerson)) + '\n'
        f.write(writestring)


#### What if 2019 ISMRM would have been in Sydney analysis ####
year = '2019'
filename = '../data/processed/%s.csv'%(year)
country = 'Australia'
nPeople = lib.nAbstractsFromCountry(filename, country)
altlongFromfile = lib.getAllLongAltFromFile(filename)
geolocator = Nominatim(user_agent = 'my_app', timeout=20)
geolocation = geolocator.geocode('Sydney, Australia', language='en', addressdetails=True)
ref_altlong = ((geolocation.latitude, geolocation.longitude))

totalmiles = 0
for altlong in altlongFromfile:
    miles = geodesic(ref_altlong, altlong).miles
    if miles > limitnoflight:
        totalmiles += miles

totalmiles *= 2
totalmiles = round(totalmiles)
nAccepted = lib.getnAccpetedAbstracts(year)
nExtract = lib.nExtracted(filename)
factor = nAccepted / nExtract
miles_perPerson = round(totalmiles / nExtract)
extrapolated_totalmiles = totalmiles * factor

totalkilometers = round(extrapolated_totalmiles * miles2kilometer)
totalco2 = totalkilometers * co2perkm_kg
co2perPerson = totalco2 / nAccepted

with open(writefile, 'a') as f:
    writestring = '2020,'+ str(nAccepted) +','+ \
        str(nExtract) +','+ str(totalmiles) +','+ \
        str(round(extrapolated_totalmiles))\
        +','+ str(miles_perPerson) +',' + str(round(totalco2)) +','+ \
        str(round(co2perPerson)) + '\n'
    f.write(writestring)

#### Analysis of how different cities around the world ####
# a set of randomly selected cities and conference locations from the last ten
# years
cities2compare = ['Cape Town, South Africa', 'Lilongwe, Malawi', \
                  'Abu Dhabi, United Arab Emirates', 'New Delhi, India', \
                  'Tokyo, Japan', 'Moscow, Russia', 'Bogota, Colombia', \
                  'Sao Paulo, Brazil', 'Buenos Aires, Argentina', \
                  'Mexico City, Mexico', 'Tehran, Iran', 'Tel Aviv-Yafo, Israel',\
                  'Munich, Germany', 'Reykjavik, Iceland', 'Anchorage, USA',\
                  'Dallas, USA', 'Lima, Peru', 'Stockholm, Sweden', \
                  'Montreal, Canada', 'Melbourne, Australia', \
                  'Salt Lake City, USA', 'Milan, Italy', 'Toronto, Canada', \
                  'Singapore', 'Honolulu, USA', 'Paris, France']

geolocator = Nominatim(user_agent = 'my_app', timeout=20)
writefile2 = '../data/processed/analysistheo.csv'
with open(writefile2, 'w') as f:
    for city in cities2compare:
        print(city)
        time.sleep(0.3)

        geolocation = geolocator.geocode(city, language='en', addressdetails=True)
        ref_altlong = ((geolocation.latitude, geolocation.longitude))
        totalmiles = 0

        for altlong in altlongFromfile:
            miles = geodesic(ref_altlong, altlong).miles
            if miles > limitnoflight:
                totalmiles += miles

        totalmiles *= 2
        totalmiles = round(totalmiles)

        miles_perPerson = round(totalmiles / nExtract)
        extrapolated_totalmiles = totalmiles * factor
        
        totalkilometers = round(extrapolated_totalmiles * miles2kilometer)
        totalco2 = totalkilometers * co2perkm_kg
        co2perPerson = totalco2 / nAccepted

        writestring = city +','+ str(nAccepted) +','+ \
            str(nExtract) +','+ str(totalmiles) +','+ \
            str(round(extrapolated_totalmiles))\
            +','+ str(miles_perPerson) +',' + str(round(totalco2)) +','+ \
            str(round(co2perPerson)) + '\n'
        f.write(writestring)

#### Analysis of how many people traveled to Australia in a year ####
writefile3 = '../data/processed/australiapercent.csv'
with open(writefile3, 'w') as f:
    for year in years:
        nAccepted = lib.getnAccpetedAbstracts(year)
        filename = '../data/processed/%s.csv'%(year)
        country = 'Australia'
        nPeople = lib.nAbstractsFromCountry(filename, country)
        percent = round(nPeople / nAccepted * 100, 2)
        print(percent)

        writestring = year +','+ str(percent) + '\n'
        f.write(writestring)

