import time
import library as lib
from geopy.geocoders import Nominatim
from geopy.distance import geodesic

def runYear(year, filename):
    # get the number of abstracts in the year
    nAbstracts = lib.getnAccpetedAbstracts(year)
    # define url to ismrm archive
    baseURL = 'HTTP://archive.ismrm.org/'
    # define url for the year
    url = baseURL + year

    # get longitude and altitude of the conference location
    ref_altlong = lib.getRefLocation(year)

    # initilaize the OpenStreetMaps API
    geolocator = Nominatim(user_agent = 'my_app', timeout=20)

    with open(filename, 'a') as f:
        for i in range(nAbstracts):

            i = i+1
            time.sleep(0.5) # wait to be conform with rate limit of geolocator API
            whilecount = 0
            abstractID = '%04d'%(i)
            print('Processing Abstract %s:'%abstractID)

            # geolocator can fail due to timeout, if necessary try it several times
            while True:
                try:
                    # if the geolocator API fails, try it up to 5 times
                    if whilecount > 4:
                        break
                    # Read the location string from the abstract from the first
                    # author
                    location = lib.getFirstAuthorLocation(url, abstractID)
                    # use the read string to localize the firstauthor with
                    # OpenStreet Maps API
                    geolocation = geolocator.geocode(location, language='en', \
                                                     addressdetails=True)
                    # extract longitude and altitude
                    altlong = ((geolocation.latitude, geolocation.longitude))
                    # measure distance of the first author to the conference
                    # using a elipsoid model of the earth
                    miles = geodesic(ref_altlong, altlong).miles
                    break
                except:
                    time.sleep(1)
                    whilecount += 1

            if whilecount > 4:
                print('Abstract with ID \'%s\' failed\n'%(abstractID))
                continue

            # make the distance of the first author to the conference a string
            # and round it to two digits
            writemiles = str(round(miles, 2))
            # extract the country from the API
            country = geolocation.raw['address']['country']

            # Shell output
            print(geolocation)
            print('Distance in miles: %s\n'%(writemiles))

            # Prepare a one liner for the output-csv file
            # Data Structure:
            # first line: abstracID,country,longitude,altitude,distance in miles
            # second line: geolocator full string of the location
            writestring = abstractID + ',' + country + ',' + str(altlong[0]) + ',' \
                + str(altlong[1]) + ',' + writemiles + '\n'
            f.write(writestring)
            f.write(str(geolocation) + '\n')


if __name__ == '__main__':
    # define the years to extract (2011 was too inconsistent)
    years = ['2010', '2012', '2013', '2014', '2015', '2016', '2017', \
             '2018', '2019'];

    # run through all years
    for year in years:
        print(year)
        filename = '../data/processed/%s.csv'%(year)
        runYear(year, filename)
