import requests
import urllib.request
import csv

from bs4 import BeautifulSoup

from geopy.distance import geodesic
from geopy.geocoders import Nominatim

import library as lib

filename = '../data/processed/capitols.csv'
csv_file = open(filename, 'r')
csv_reader = csv.reader(csv_file, delimiter=',')

cities = []
line_count = 0
for row in csv_reader:
    line_count += 1
    if line_count > 0:
        city = [row[0], row[1]]

    cityMani = ", ".join(city)
    cities.append(cityMani)

geolocator = Nominatim(user_agent = 'my_app', timeout=20)

year = '2019'
filename = '../data/processed/%s.csv'%(year)
altlongFromfile = lib.getAllLongAltFromFile(filename)
nAccepted = lib.getnAccpetedAbstracts(year)
nExtract = lib.nExtracted(filename)
factor = nAccepted / nExtract
miles2kilometer = 1.60934
co2perkm_kg = 0.211 # see literature.bib file 'Co2permile2'

limitnoflight = 300 # limit for short distances
writefile = '../data/processed/captiolsanalysis.csv'

with open(writefile, 'w') as f:
    for city in cities:
        geolocation = geolocator.geocode(city, language='en', addressdetails=True)
        if not geolocation is None:
            ref_altlong = ((geolocation.latitude, geolocation.longitude))
            totalmiles = 0

            for altlong in altlongFromfile:
                miles = geodesic(ref_altlong, altlong).miles
                if miles > limitnoflight:
                    totalmiles += miles

            totalmiles *= 2
            totalmiles = round(totalmiles)

            miles_perPerson = round(totalmiles / nExtract)
            extrapolated_totalmiles = totalmiles * factor

            totalkilometers = round(extrapolated_totalmiles * miles2kilometer)
            totalco2 = totalkilometers * co2perkm_kg
            co2perPerson = totalco2 / nAccepted
            print(city)
            print(totalmiles)

            writestring = city +','+ str(nAccepted) +','+ \
                str(nExtract) +','+ str(totalmiles) +','+ \
                str(round(extrapolated_totalmiles))\
                +','+ str(miles_perPerson) +',' + str(round(totalco2)) +','+ \
                str(round(co2perPerson)) + '\n'
            f.write(writestring)
